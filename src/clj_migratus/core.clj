(ns clj-migratus.core
  (:require
   [clojure.java.io :as io]
   [migratus.core :as migratus]))

(defn load-config
  []
  (let [config-file (io/file "migratus.edn")]
    (if (.exists config-file)
      (-> (slurp "migratus.edn")
          clojure.edn/read-string)
      (throw (Exception. "migratus.edn not found")))))

(defn init
  [config]
  (migratus/init config))

(defn migrate
  [config]
  (migratus/migrate config))

(defn up
  [config]
  (migratus/up config))

(defn down
  [config]
  (migratus/down config))

(defn create
  [config name]
  (migratus/create config name))

(defn rollback
  [config]
  (migratus/rollback config))

(defn pending-list
  [config]
  (migratus/pending-list config))

(defn migrate-until-just-before
  [config]
  (migratus/migrate-until-just-before))

(defn -main
  [& args]
  (let [arg (first args)
        config (load-config)]
    (cond
      (= arg "init") (init config)
      (= arg "migrate") (migrate config)
      (= arg "up") (up config (rest args))
      (= arg "down") (down config (rest args))
      (= arg "create") (create config (second args))
      (= arg "rollback") (rollback config)
      (= arg "pending-list") (pending-list config)
      (= arg "migrate-until-just-before") (migrate-until-just-before (rest args))
      :else
      (throw (Exception. (str "Command not found " arg))))))
